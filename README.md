# Iceberg Distribution Algorithm
This repository is on the research in Greenland Ice Sheet. 
The project is about icebergs in a 200 km coastal stretch around Greenland Ice Sheet. We are using Sentinel-1 SAR
image to identify and segment icebergs. The images we use are at spatial resolution of 40 meters. This segmentation is initially done in the Constant False Alarm Rate (CFAR)
algorithm with primary and secondary thresholds. These thresholds help in identifying the pixels of the icebergs. Region
growing is implemented on the brightest of the pixels and the growing is done as a 4 connected component.

Once all the icebergs are identified and segmented, their areas and coordinates are outputed into a csv file. 
This is used to do statistical analysis on the data and relate with climatic and cryospheric changes around the 
Greenland Ice Sheet.

The minimum size of icebergs which is being considered is 3 pixels.

We are looking into considering the high resolution images at 10 m, especially in the coastal regions as the icebergs
are smaller and will have greater accuracy for segmenting the smaller as well as larger icebergs. After the initial
region close to the coast, the remaining region would be with medium resolution images at 40 meters.

# Update to image resolution
The new resolution is Sentinel-1 A/B with GRD 20 meter spatial resolution and pixel spacing of 10 meter. The resolution has improved the shape and size 
prediction with the CFAR (N-sigma) algorithm.There are certain regions in NorthEast, East, and West Greenland that require
coarser resolution, since the high resolution does not have the coverage.

The texture of the SAR image is calculated and decibel values of the ASM, Dissimilarity, and Variance are used to create a 
composite. The composite is then inputted into the k-means segmentation algorithm that outputs a binary image with projection
metadata associated with it.



