"""
Description: Automated download of Sentinel-1 image tiles using ESA Sentinel Scihub API - wget with OpenSearch.

 The URI is defined using the point feature as a footprint for each image tile. This helps
 to avoid multiple image tiles to be picked up at a particular orbit ID. 
 
 The point LAT (0,90) and LONG (-180, +180) values need to be extracted only once for a particular tile. 
 The next iteration of the tile can be found by incrementing or decrementing the Orbit ID by 175. 
 The LAT-LONG values are displayed as (LONG, LAT) in the scihub portal. 
 So it needs to be swapped before adding to the URI.
 
 In order to avoid finding tiles back in time, which includes substracting the Orbit ID by 175,
 the tiles are used from the very beginning of image acquisition for Sentinel - S1A, S1B.
 
 This can be improved by using a SWITCH case condition, in which the user will be asked for
 a choice if you want to look at tiles from previous iterations.
             


sample URI: https://scihub.copernicus.eu/dhus/search?q=S1A_EW_GRDM_1SDH_*_*_005572_*_* AND footprint:Intersects(65.6962, -39.7299)
https://scihub.copernicus.eu/dhus/search?q=S1A_EW_GRDM_1SDH_*_*_005572_*_*%20AND%20footprint:%22Intersects(65.6962,%20-39.7299)%22   
@author: Siddharth Shankar
"""

import wget
import getpass
import lxml
import urllib
from xml.etree import ElementTree as ET
import requests
from xml.dom import minidom
import urllib2
import requests
from urllib import quote,urlretrieve
from urllib2 import urlopen

#Orbits per cycle
orbitsPerCycle = 175

#Sample orbit number
Orb1 = 14437
Orb2 = 5572
# Converting the orbit number into 6 digit numbers
OrbitNumber = format(Orb1,"06")

# URI for the xml response which houses all the attributes of the tile for download over AOI
uri = str('https://scihub.copernicus.eu/dhus/search?q=S1A_EW_GRDM_1SDH_*_*_'+OrbitNumber+'_*_*%20AND%20footprint:"Intersects(82.8656,-30.0266)"')

#xml = wget.download(uri, out ="/Users/Sid/Downloads/s.json")

#Reading the https response from the URI which is in xml format
response = urllib.urlopen(uri)
data = response.read()
print data

# Parsing the xml response to identify the UUID for tile download and IDENTIFIER for filename
dom = minidom.parseString(data)
for s in dom.getElementsByTagName('str'):
    if s.getAttribute('name') == 'identifier':
        fileName = str(s.childNodes[0].data) # Accessing the substring which holds the identifier
    if s.getAttribute('name') == 'uuid':
#        print s.childNodes[0].data
        uuid = str(s.childNodes[0].data) # Accessing the substring which holds the uuid
        uuidStr = "'%s'"%uuid
        imgStr = ('https://scihub.copernicus.eu/dhus/odata/v1/Products('+uuidStr+')/$value')
        wget.download(imgStr, out = "/Users/Sid/Downloads/"+fileName+".zip") #Download the file in local drive using wget module
        
        







